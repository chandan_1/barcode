import React, { Component } from 'react';
import { View, Text,ImageBackground } from 'react-native';
import firebase from 'react-native-firebase';
import {Input,Form, Item, Title,Button} from 'native-base';
import CameraS from './CameraS';
import Home from './Home';
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {

        email:'iam@gmail.com',
        password:'123456',
    };
  }
  login()
  {
    firebase.auth().signInWithEmailAndPassword(this.state.email,this.state.password)
    .then((data)  => {
      this.props.navigation.navigate('Home');
    })
    .catch((err) => {
      alert(err);
    })
  }
  render() {
    return (
      <View style={{}}>
        <ImageBackground
        style={{height:700,justifyContent:'center',alignItems: 'center',}}
        source={require('./1.jpg' )}
        >

<View style={{ backgroundColor:'white',borderRadius:5,
        shadowColor: "#000",
        shadowOffset: {	width: 0,height: 9,},
        shadowOpacity: 0.48,
        shadowRadius: 11.95,
        elevation: 18,
        height:250,
        width:340,
        }}>
          <Text
          style={{fontSize:30,alignSelf:'center'}}>
               Login
          </Text>
          <Form>
        <Item>
           
        <Input 

        value={this.state.email}
        onChangeText={(text) => this.setState({email:text})}
        style={{paddingTop:10,borderBottomColor:'black',alignSelf:'center',borderBottomWidth:0.5}}/>
        
        </Item>
        <Item>
          
        <Input 
        value={this.state.password}
        secureTextEntry
        onChangeText={(text) => this.setState({password:text})}
        style={{borderBottomColor:'black',alignSelf:'center',borderBottomWidth:0.5}}/>
        </Item>
        </Form>
        <View
        style={{paddingLeft: 90,paddingTop:15}}>
        <Button
        title="Login"
        onPress={() => this.login()}
        style={{width:150,borderRadius: 20,}}
        />
        </View>
        <View><Button/></View>
        </View>

        </ImageBackground>

        </View>
    );
  }
}

export default Login;
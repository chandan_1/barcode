import React, { Component } from 'react';
import { View, Text,Linking,Button,TouchableOpacity } from 'react-native';
import {Header} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { WebView } from 'react-native-webview';

class CartPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.arr = [];
    this.url='google.com';
    this.total = 0;
    this.prize2=0;
  }
  getData = async () => {
    let key = await AsyncStorage.getAllKeys();
    let i;
    for( i in key){
        let data = await AsyncStorage.getItem(key[i]);
        let jsondata = JSON.parse(data);
        let p = parseInt(jsondata.prize);
        let d= parseInt(jsondata.quantity);
        this.prize2 =p*d
        p=this.prize2
        this.total += p;
        this.arr.push(jsondata);
    }
    this.setState({abcd:''})
  }
  cleardata(){
      AsyncStorage.clear();
  }
  componentDidMount(){
    //   this.cleardata();
      this.getData();
  }
  render() {
    return (
      <View>
            <View style={{width:400,height:150,backgroundColor:'skyblue'}}>
                <Text 
                
                style={{fontSize:20,fontWeight:'bold'}}>
                    Kewal Krishan Shiv Kumar
                </Text>
                <Text
                style={{fontSize:17}}
                >
                    (Wholesale Karyana) 
                </Text> 
            </View>


            <View style={{flexDirection:'row'}}>
                <View
                style={{borderWidth: 1,borderColor:'black'}}
                >
                <Header>
                <Text>
                Name of the product  
                </Text>
                <Text>            </Text>
                </Header>
                {this.arr.map((item) => {
                return <Text>{item.name}</Text>
                
                })}

                </View>
                
                <View
                style={{borderRightWidth: 1,borderBottomWidth:1,borderRightColor:'black',borderTopRadius:1}}
                >
                <Header>
                <Text>
                Weight
                </Text>
                </Header>    
                {this.arr.map((item) => {
                return 
                <Text
                style={{fontWeight:'bold'}}
                >{item.weight}</Text>
                })}
                </View>
                <View
                style={{borderRightWidth: 1,borderBottomWidth:1,borderRightColor:'black',borderTopRadius:1}}
                >
                <Header>
                <Text>
                 Quantity
                </Text>
                </Header>    
                {this.arr.map((item) => {
                return <Text>{item.quantity}</Text>
                })}
                </View>
                <View
                style={{borderRightWidth: 1,borderBottomWidth:1,borderRightColor:'black',borderTopRadius:1}}
                >
                <Header>
                <Text>
                 Prize
                </Text>
                </Header>    
                {this.arr.map((item) => {
                return <Text>{item.prize * item.quantity}</Text>
                })}

                </View>

                
          </View>
                <View
                style={{borderTopWidth:1,flexDirection:'row',borderBottomWidth:1,}}
                >
                    <Text style={{fontWeight:'bold'}}>Total :                                                                                     </Text>
                    <Text style={{fontWeight:'bold'}}>{this.total}</Text>
                </View>
            <Button
            title='aibad'
                onPress={()=> this.props.navigation.navigate('Webviewpage', {total:this.total})}
            />
  
      </View>
    );
  } 
}

export default CartPage;

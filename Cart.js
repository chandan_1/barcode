import React, { Component } from 'react';
import { Text, View, Linking, TextInput, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet,Button} from 'react-native';
import { CameraKitCameraScreen, } from 'react-native-camera-kit';
import firebase from 'react-native-firebase';
import showcartitem from './showcartitem';
import CartPage from './CartPage';
class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
        qrvalue: '',
      opneScanner: false,
    };
  }
  input(){
      if(this.state.qrvalue!='')
      {
        this.props.navigation.navigate('showcartitem', {
            qrvalue:this.state.qrvalue
        })   
      }
      else{
          alert('j')
      }
  }
  onOpenlink() {
    //Function to open URL, If scanned 
    Linking.openURL(this.state.qrvalue);
    //Linking used to open the URL in any browser that you have installed
  }
  onBarcodeScan(qrvalue) {
    //called after te successful scanning of QRCode/Barcode
    this.setState({ qrvalue: qrvalue });
    this.setState({ opneScanner: false });
  }
  onOpneScanner() {
    var that =this;
    //To Start Scanning
    if(Platform.OS === 'android'){
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,{
              'title': 'CameraExample App Camera Permission',
              'message': 'CameraExample App needs access to your camera '
            }
          )
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //If CAMERA Permission is granted
            that.setState({ qrvalue: '' });
            that.setState({ opneScanner: true });
          } else {
            alert("CAMERA permission denied");
          }
        } catch (err) {
          alert("Camera permission err",err);
          console.warn(err);
        }
      }
      //Calling the camera permission function
      requestCameraPermission();
    }else{
      that.setState({ qrvalue: '' });
      that.setState({ opneScanner: true });
    }    
  }


  render() {
    let displayModal;
    //If qrvalue is set then return this view
    if (!this.state.opneScanner) {
    return (
        <View style={styles.container}>
            <Button
            title='checkout'
            onPress={() => this.props.navigation.navigate('CartPage')}
            />
            <Text style={styles.heading}>React Native QR Code Example</Text>
            <TextInput 
                value={this.state.qrvalue}
                onChangeText= {(t) =>this.setState({qrvalue:t})}
                placeholder="QR VALUE"
            />
            
            <TouchableHighlight
              onPress={() => this.onOpneScanner()}
              style={styles.button}>
                <Text style={{ color: '#FFFFFF', fontSize: 12 }}>
                Open QR Scanner
                </Text>
            </TouchableHighlight>
            <Button
            onPress={() => this.input() }
            title='input'
            />
        </View>
      );
    }
    return (
        <View style={{ flex: 1 }}>
          <CameraKitCameraScreen
            showFrame={false}
            //Show/hide scan frame
            scanBarcode={true}
            //Can restrict for the QR Code only
            laserColor={'blue'}
            //Color can be of your choice
            frameColor={'yellow'}
            //If frame is visible then frame color
            colorForScannerFrame={'black'}
            //Scanner Frame color
            onReadCode={event =>
              this.onBarcodeScan(event.nativeEvent.codeStringValue)
            }
          />
        </View>
      );
         
  }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor:'white'
    },
    button: {
      alignItems: 'center',
      backgroundColor: '#2c3539',
      padding: 10,
      width:300,
      marginTop:16
    },
    heading: { 
      color: 'black', 
      fontSize: 24, 
      alignSelf: 'center', 
      padding: 10, 
      marginTop: 30 
    },
    simpleText: { 
      color: 'black', 
      fontSize: 20, 
      alignSelf: 'center', 
      padding: 10, 
      marginTop: 16
    }
  });

export default Cart;
import gradiant from './gradiant';
import Home from './Home';
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import firebase from 'react-native-firebase';
import Login from './Login';
import Signup from './Signup';
import Welcome from './Welcome';
import CameraS from './CameraS';
import showdata from './showdata';
import Search from './Search';
import input from './input';
import Cart from './Cart';
import CartPage from './CartPage';
import Webviewpage from './Webviewpage';
import showcartitem from './showcartitem';
import { createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack'
import { createSwitchNavigator,createAppContainer } from 'react-navigation'
import addproduct from './addproduct';


class App extends Component {
  constructor(props) {
 
   super(props);
    this.state = {
    };
  }

  render() { 
    return (
      <View>
        <Text> App </Text>
      </View>
    );
    
  }
}
// const nav1 = createSwitchNavigator({
// Home,input 
// })
// const nav = createDrawerNavigator({
//   Home,input
// } ,{
//   contentComponent :  props => <Dash {...props}/>
// })
const navigation = createStackNavigator({
  gradiant,
  Welcome,
  Login,
  Signup,
  CameraS,
  addproduct,
  Search,
  Home,
  showdata,
  Cart,
  showcartitem,
  CartPage,
  Webviewpage
})


// export default createAppContainer(navigation);
export default gradiant;
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import WebView from 'react-native-webview'
class Webviewpage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      total:''
    };
    this.orderid = Math.round(Math.random() * 4578654647987987465);
  }
  checkpayment(){
    let url = 'http://sachtechsolution.pe.hu/easypayment/checkstatus.php?orderid='+this.orderid;
    fetch(url).then((r) => r.json())
    .then((res) => {
      if(res.status == 'success'){
        this.props.navigation.navigate('CartPage');
      }
    })
  }
  componentDidMount(){
    let total = this.props.navigation.getParam('total');
    this.setState({total:total})
  }
  render() {
    return (
      <View style={{flex:1}}>
        <WebView
          source={{uri:'http://sachtechsolution.pe.hu/easypayment/paypaytm.php?mid=NQLFiw31573709089094&amount='+this.state.total+'&adminemail=SONYMITTAL71%40GMAIL.COM&adminmobile=8728062879&mkey=a%26kNv0J3PC9kCu_O&orderid='+this.orderid}}
          onLoad={() => this.checkpayment()}
        />
      </View>
    );
  }
}

export default Webviewpage;
